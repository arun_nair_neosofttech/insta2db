import requests
from bs4 import BeautifulSoup
import psycopg2
import sys

username = str(sys.argv[1])

conn = psycopg2.connect(database="postgres",
                        host="localhost",
                        user="postgres",
                        port="5432",
                        password="root")
cur = conn.cursor()

url = "https://www.instagram.com/" + username
source = requests.get(url)

if source.status_code != 200:
    print("User account doesn't exist!")
else:
    page = BeautifulSoup(source.content, 'html.parser')
    meta = page.find('meta', attrs={"property": "og:description"})
    data = meta.get('content').split()
    user_handle = data[-1][1:-1]
    followers = data[0]
    following = data[2]
    posts = data[4]
    record = (user_handle, followers, following, posts)

    with conn:
        query = "INSERT INTO insta_users (username, followers, following, posts) VALUES (%s, %s, %s, %s);"
        cur.execute(query, record)
